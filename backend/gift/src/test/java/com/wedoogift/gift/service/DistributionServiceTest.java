package com.wedoogift.gift.service;

import com.wedoogift.gift.exception.NotAllowedOperationException;
import com.wedoogift.gift.exception.NotFoundException;
import com.wedoogift.gift.repository.CompanyRepository;
import com.wedoogift.gift.repository.DistributionRepository;
import com.wedoogift.gift.repository.UserRepository;
import com.wedoogift.gift.repository.model.*;
import com.wedoogift.gift.controller.request.DistributionRequest;
import com.wedoogift.gift.service.model.DistributionModel;
import com.wedoogift.gift.service.model.WalletType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


class DistributionServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private DistributionRepository distributionRepository;

    @InjectMocks
    private DistributionService distributionService;

    private UserEntity defaultUserEntity = null;
    private CompanyEntity defaultCompanyEntity = null;
    private DistributionRequest defaultRequest = null;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        BalanceEntity defaultBalanceEntity = createBalanceEntity(new BigDecimal(37), WalletType.FOOD);
        defaultUserEntity = createUserEntity(1, defaultBalanceEntity);
        defaultCompanyEntity = createCompanyEntity(1, "test", new BigDecimal(37));

        defaultRequest = createDistributionRequest(1, BigDecimal.TEN, LocalDate.now(), 1, WalletType.GIFT.getId());

        when(userRepository.findById(1)).thenReturn(Optional.of(defaultUserEntity));
        when(companyRepository.findById(1)).thenReturn(Optional.of(defaultCompanyEntity));

        var distributionEntityCaptor = ArgumentCaptor.forClass(DistributionEntity.class);
        when(distributionRepository.save(distributionEntityCaptor.capture())).thenAnswer(a -> distributionEntityCaptor.getValue());
    }

    @Test
    public void addDistributionReturnNotFoundExceptionWhenUserIsNotFound(){
        when(userRepository.findById(1)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> {
            distributionService.addDistribution(defaultRequest);
        }).isInstanceOf(NotFoundException.class)
                .hasMessageContaining("User not found");

    }

    @Test
    public void addDistributionReturnNotFoundExceptionWhenCompanyIsNotFound() {
        when(companyRepository.findById(1)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> {
            distributionService.addDistribution(defaultRequest);
        }).isInstanceOf(NotFoundException.class)
                .hasMessageContaining("Company not found");

    }

    @Test
    public void addDistributionReturnNotAllowedExceptionWhenBalanceIsLessOrEqualToZero() {
        defaultCompanyEntity.setBalance(BigDecimal.ZERO);
        assertThatThrownBy(() -> {
            distributionService.addDistribution(defaultRequest);
        }).isInstanceOf(NotAllowedOperationException.class)
                .hasMessageContaining("Not allowed operation");
    }

    @Test
    public void addDistributionReturnNotFoundExceptionWhenWalletIsNotFound(){
        DistributionRequest request = createDistributionRequest(1, BigDecimal.TEN, LocalDate.now(), 1, 3);
        assertThatThrownBy(() -> {
            distributionService.addDistribution(request);
        }).isInstanceOf(NotFoundException.class)
                .hasMessageContaining("Wallet is not found");
    }

    @Test
    public void addDistributionReturnNotAllowedExceptionWhenDateForFoodGiftIsTooLate(){
        LocalDate startDate = LocalDate.of(2018, 1,1);
        DistributionRequest request = createDistributionRequest(1, BigDecimal.TEN, startDate, 1, WalletType.FOOD.getId());

        assertThatThrownBy(() -> {
            distributionService.addDistribution(request);
        }).isInstanceOf(NotAllowedOperationException.class)
                .hasMessageContaining("meal gift can't be distributed");

    }

    @Test
    public void addDistributionReturnDistributionModelWhenGiftType() {
        DistributionModel result = distributionService.addDistribution(defaultRequest);

        verify(userRepository).save(defaultUserEntity);
        verify(companyRepository).save(defaultCompanyEntity);
        assertThat(result.getUserId()).isEqualTo(defaultUserEntity.getId());
        assertThat(result.getCompanyId()).isEqualTo(defaultCompanyEntity.getId());
        assertThat(result.getWalletId()).isEqualTo(WalletType.GIFT.getId());
        assertThat(result.getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(result.getStartDate()).isEqualTo(defaultRequest.getStartDate());

        assertThat(result.getEndDate()).isEqualTo(defaultRequest.getStartDate().plusYears(1));
    }

    @Test
    public void addDistributionReturnDistributionModelWhenFoodType() {
        DistributionRequest request = createDistributionRequest(1, BigDecimal.TEN, LocalDate.now(), 1, WalletType.FOOD.getId());
        DistributionModel result = distributionService.addDistribution(request);

        verify(userRepository).save(defaultUserEntity);
        verify(companyRepository).save(defaultCompanyEntity);
        assertThat(result.getUserId()).isEqualTo(defaultUserEntity.getId());
        assertThat(result.getCompanyId()).isEqualTo(defaultCompanyEntity.getId());
        assertThat(result.getWalletId()).isEqualTo(WalletType.FOOD.getId());
        assertThat(result.getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(result.getStartDate()).isEqualTo(defaultRequest.getStartDate());

        LocalDate endDate = defaultRequest.getStartDate().plusYears(1).withMonth(2);
        assertThat(result.getEndDate()).isEqualTo(endDate.withDayOfMonth(endDate.lengthOfMonth()));
    }

    private DistributionRequest createDistributionRequest(int userId, BigDecimal amount, LocalDate startDate, int companyId, int walletId) {
        DistributionRequest request = new DistributionRequest();
        request.setUserId(userId);
        request.setAmount(amount);
        request.setStartDate(startDate);
        request.setCompanyId(companyId);
        request.setWalletId(walletId);
        return request;
    }

    private UserEntity createUserEntity(int id, BalanceEntity balance) {
        List<BalanceEntity> list = new ArrayList<>();
        list.add(balance);

        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        userEntity.setBalances(list);
        return userEntity;
    }

    private CompanyEntity createCompanyEntity(int id, String name, BigDecimal amount) {
        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setId(id);
        companyEntity.setName(name);
        companyEntity.setBalance(amount);
        return companyEntity;
    }

    private BalanceEntity createBalanceEntity(BigDecimal amount, WalletType wallet) {
        BalanceEntity balanceEntity = new BalanceEntity();
        balanceEntity.setAmount(amount);
        balanceEntity.setWallet(wallet);
        return balanceEntity;
    }

}