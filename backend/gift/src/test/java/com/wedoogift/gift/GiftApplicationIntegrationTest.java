package com.wedoogift.gift;

import com.wedoogift.gift.GiftApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = GiftApplication.class)
@AutoConfigureMockMvc
class GiftApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @WithMockUser(value = "user", password = "password")
    @Test
    void whenGetGiftInformationReturn200() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/gift")
                .contentType("application/json")
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }
}