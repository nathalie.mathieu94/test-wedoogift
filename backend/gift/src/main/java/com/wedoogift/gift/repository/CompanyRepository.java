package com.wedoogift.gift.repository;

import com.wedoogift.gift.repository.model.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<CompanyEntity, Integer> {
}
