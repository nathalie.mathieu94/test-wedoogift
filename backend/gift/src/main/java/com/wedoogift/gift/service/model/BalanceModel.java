package com.wedoogift.gift.service.model;

import java.math.BigDecimal;

public class BalanceModel {

    private int walletId;
    private BigDecimal amount;

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
