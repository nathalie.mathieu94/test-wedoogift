package com.wedoogift.gift.service.model;

import java.util.List;
import java.util.Objects;

public class GiftModel {
    private List<CompanyModel> companies;
    private List<UserModel> users;
    private List<DistributionModel> distributions;

    public List<CompanyModel> getCompanies() {
        return companies;
    }

    public void setCompanies(List<CompanyModel> companies) {
        this.companies = companies;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    public List<DistributionModel> getDistributions() {
        return distributions;
    }

    public void setDistributions(List<DistributionModel> distributions) {
        this.distributions = distributions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiftModel that = (GiftModel) o;
        return Objects.equals(companies, that.companies) && Objects.equals(users, that.users) && Objects.equals(distributions, that.distributions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companies, users, distributions);
    }
}
