package com.wedoogift.gift.repository.model;

import com.wedoogift.gift.service.model.WalletType;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name= "BALANCE")
public class BalanceEntity {
    @Id
    @GeneratedValue
    private Integer id;
    private WalletType wallet;
    private BigDecimal amount;
    @ManyToOne
    private UserEntity user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletType getWallet() {
        return wallet;
    }

    public void setWallet(WalletType wallet) {
        this.wallet = wallet;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
