package com.wedoogift.gift.repository.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name ="USER")
public class UserEntity {
    @Id
    @GeneratedValue
    private Integer id;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<BalanceEntity> balances;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<BalanceEntity> getBalances() {
        return balances;
    }

    public void setBalances(List<BalanceEntity> balances) {
        this.balances = balances;
    }
}
