package com.wedoogift.gift.controller;

import com.wedoogift.gift.controller.request.CompanyRequest;
import com.wedoogift.gift.controller.request.DistributionRequest;
import com.wedoogift.gift.controller.request.UserRequest;
import com.wedoogift.gift.service.CompanyService;
import com.wedoogift.gift.service.DistributionService;
import com.wedoogift.gift.service.GiftService;
import com.wedoogift.gift.service.UserService;
import com.wedoogift.gift.service.model.CompanyModel;
import com.wedoogift.gift.service.model.DistributionModel;
import com.wedoogift.gift.service.model.GiftModel;
import com.wedoogift.gift.service.model.UserModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GiftApplicationController {

    private final GiftService giftService;
    private final DistributionService distributionService;
    private final UserService userService;
    private final CompanyService companyService;

    public GiftApplicationController(GiftService giftService,
                                     DistributionService distributionService,
                                     UserService userService,
                                     CompanyService companyService) {
        this.giftService = giftService;
        this.distributionService = distributionService;
        this.userService = userService;
        this.companyService = companyService;
    }

    @PostMapping("/users")
    public UserModel createUser(@RequestBody UserRequest request) {
        return userService.addUser(request);
    }

    @PostMapping("/companies")
    public CompanyModel createCompany(@RequestBody CompanyRequest request) {
        return companyService.addCompany(request);
    }

    @PostMapping("/distributions")
    public DistributionModel createDistribution(@RequestBody DistributionRequest request) {
        return distributionService.addDistribution(request);
    }

    @GetMapping("/gift")
    public GiftModel getGift() {
        return giftService.getGift();
    }

}
