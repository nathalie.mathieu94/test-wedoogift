package com.wedoogift.gift.service;

import com.wedoogift.gift.exception.NotAllowedOperationException;
import com.wedoogift.gift.exception.NotFoundException;
import com.wedoogift.gift.repository.*;
import com.wedoogift.gift.repository.model.*;
import com.wedoogift.gift.controller.request.DistributionRequest;
import com.wedoogift.gift.service.model.DistributionModel;
import com.wedoogift.gift.service.model.WalletType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DistributionService {

    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final DistributionRepository distributionRepository;

    public DistributionService(UserRepository userRepository,
                               CompanyRepository companyRepository,
                               DistributionRepository distributionRepository) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.distributionRepository = distributionRepository;
    }

    public List<DistributionModel> findAllDistributions() {
        List<DistributionEntity> distributionEntities = distributionRepository.findAll();
        return distributionEntities.stream()
                .map(this::toModel)
                .collect(Collectors.toList());
    }

    @Transactional
    public DistributionModel addDistribution(DistributionRequest request) throws NotFoundException {
        UserEntity userEntity = userRepository.findById(request.getUserId())
                                    .orElseThrow(() -> new NotFoundException("User not found"));

        CompanyEntity companyEntity = companyRepository.findById(request.getCompanyId())
                                        .orElseThrow(() -> new NotFoundException("Company not found"));

        BigDecimal requestAmount = request.getAmount();
        validateIfBalanceCompanyIsMoreThanZero(companyEntity, requestAmount);

        WalletType wallet = WalletType.findById(request.getWalletId())
                                    .orElseThrow(() -> new NotFoundException("Wallet is not found"));

        if (wallet == WalletType.FOOD) {
            validateIfMealGiftCanBeDistributed(request);
        }
        DistributionEntity distributionEntityToSave = createDistributionEntity(request, userEntity, companyEntity, wallet);
        DistributionEntity distributionEntity = distributionRepository.save(distributionEntityToSave);

        calculateNewUserBalance(userEntity, requestAmount, wallet);
        calculateNewCompanyBalance(companyEntity, requestAmount);

        return toModel(distributionEntity);
    }

    private void validateIfMealGiftCanBeDistributed(DistributionRequest request) {
        LocalDate endRequestDate = calculateEndDateForMealGift(request.getStartDate());
        if (LocalDate.now().isAfter(endRequestDate)) {
            throw new NotAllowedOperationException("meal gift can't be distributed");
        }
    }

    private LocalDate calculateEndDateForMealGift(LocalDate date) {
        LocalDate endDate = date.plusYears(1).withMonth(2);
        return endDate.withDayOfMonth(endDate.lengthOfMonth());
    }
    private void validateIfBalanceCompanyIsMoreThanZero(CompanyEntity companyEntity, BigDecimal amount) {
        BigDecimal balance = companyEntity.getBalance();
        if (balance.subtract(amount).compareTo(BigDecimal.ZERO) <= 0) {
            throw new NotAllowedOperationException("Not allowed operation");
        }
    }

    private DistributionEntity createDistributionEntity(DistributionRequest request,
                                                        UserEntity userEntity,
                                                        CompanyEntity companyEntity,
                                                        WalletType walletType) {
        DistributionEntity distributionEntity = new DistributionEntity();
        distributionEntity.setUser(userEntity);
        distributionEntity.setCompany(companyEntity);
        distributionEntity.setWallet(walletType);
        distributionEntity.setAmount(request.getAmount());
        LocalDate startDate = request.getStartDate();
        distributionEntity.setStartDate(startDate);
        LocalDate endDate;
        if (walletType == WalletType.GIFT) {
            endDate = startDate.plusYears(1);
        } else {
            endDate =  calculateEndDateForMealGift(startDate);
        }
        distributionEntity.setEndDate(endDate);
        return distributionEntity;
    }

    private void calculateNewUserBalance(UserEntity userEntity, BigDecimal amount, WalletType walletType) {
        var balance = userEntity.getBalances().stream()
                .filter(b -> b.getWallet().equals(walletType))
                .findAny().orElseGet(()-> {
                    var newBalance = new BalanceEntity();
                    newBalance.setWallet(walletType);
                    newBalance.setAmount(BigDecimal.ZERO);
                    userEntity.getBalances().add(newBalance);
                    return newBalance;
                });

        BigDecimal newAmount = balance.getAmount().add(amount);
        balance.setAmount(newAmount);

        userRepository.save(userEntity);
    }

    private void calculateNewCompanyBalance(CompanyEntity companyEntity, BigDecimal amount) {
        BigDecimal oldBalance = companyEntity.getBalance();
        BigDecimal newBalance = oldBalance.subtract(amount);
        companyEntity.setBalance(newBalance);

        companyRepository.save(companyEntity);
    }

    private DistributionModel toModel(DistributionEntity distributionEntity) {
        DistributionModel model = new DistributionModel();
        model.setId(distributionEntity.getId());
        model.setUserId(distributionEntity.getUser().getId());
        model.setCompanyId(distributionEntity.getCompany().getId());
        model.setAmount(distributionEntity.getAmount());
        model.setStartDate(distributionEntity.getStartDate());
        model.setEndDate(distributionEntity.getEndDate());
        model.setWalletId(distributionEntity.getWallet().getId());
        return model;
    }
}
