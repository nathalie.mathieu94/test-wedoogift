package com.wedoogift.gift.service;

import com.wedoogift.gift.controller.request.UserRequest;
import com.wedoogift.gift.exception.NotFoundException;
import com.wedoogift.gift.repository.UserRepository;
import com.wedoogift.gift.repository.model.BalanceEntity;
import com.wedoogift.gift.repository.model.UserEntity;
import com.wedoogift.gift.service.model.BalanceModel;
import com.wedoogift.gift.service.model.UserModel;
import com.wedoogift.gift.service.model.WalletType;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserModel> findAllUsers() {
        List<UserEntity> userEntities = userRepository.findAll();
        return userEntities.stream()
                .map(this::toModel)
                .collect(Collectors.toList());
    }

    public UserModel addUser(UserRequest request) {
        UserEntity userEntity = new UserEntity();
        List<BalanceEntity> balanceEntities = request.getBalances().stream()
                .map(balanceModel -> {
                    BalanceEntity balanceEntity = new BalanceEntity();
                    balanceEntity.setAmount(balanceModel.getAmount());
                    balanceEntity.setWallet(WalletType.findById(balanceModel.getWalletId())
                            .orElseThrow(() -> new NotFoundException("Wallet is not found")));
                    balanceEntity.setUser(userEntity);
                    return balanceEntity;
                }).collect(Collectors.toList());
        userEntity.setBalances(balanceEntities);
        return toModel(userRepository.save(userEntity));
    }

    private UserModel toModel(UserEntity userEntity) {
        UserModel userModel = new UserModel();
        userModel.setId(userEntity.getId());
        userModel.setBalances(userEntity.getBalances().stream()
                .map(this::toModel).collect(Collectors.toList()));
        return userModel;
    }

    private BalanceModel toModel(BalanceEntity balanceEntity) {
        BalanceModel balanceModel = new BalanceModel();
        balanceModel.setWalletId(balanceEntity.getWallet().getId());
        balanceModel.setAmount(balanceEntity.getAmount());
        return balanceModel;
    }
}
