package com.wedoogift.gift.service.model;

import java.util.List;
import java.util.Objects;

public class UserModel {
    private int id;
    private List<BalanceModel> balances;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<BalanceModel> getBalances() {
        return balances;
    }

    public void setBalances(List<BalanceModel> balances) {
        this.balances = balances;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserModel userModel = (UserModel) o;
        return id == userModel.id && Objects.equals(balances, userModel.balances);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balances);
    }
}
