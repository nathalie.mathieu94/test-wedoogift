package com.wedoogift.gift.repository;

import com.wedoogift.gift.repository.model.DistributionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistributionRepository extends JpaRepository<DistributionEntity, Integer> {
}
