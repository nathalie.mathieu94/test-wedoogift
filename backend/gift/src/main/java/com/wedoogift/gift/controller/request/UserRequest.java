package com.wedoogift.gift.controller.request;

import com.wedoogift.gift.service.model.BalanceModel;

import java.util.List;

public class UserRequest {
    private List<BalanceModel> balances;

    public List<BalanceModel> getBalances() {
        return balances;
    }

    public void setBalances(List<BalanceModel> balances) {
        this.balances = balances;
    }
}
