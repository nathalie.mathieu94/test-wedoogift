package com.wedoogift.gift.repository.model;

import com.wedoogift.gift.service.model.WalletType;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Repository
@Entity(name = "Distribution")
public class DistributionEntity {

    @Id
    @GeneratedValue
    private Integer id;

    private BigDecimal amount;
    private LocalDate startDate;
    private LocalDate endDate;
    private WalletType wallet;

    @ManyToOne
    private CompanyEntity company;

    @ManyToOne
    private UserEntity user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public CompanyEntity getCompany() {
        return company;
    }

    public void setCompany(CompanyEntity company) {
        this.company = company;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public WalletType getWallet() {
        return wallet;
    }

    public void setWallet(WalletType wallet) {
        this.wallet = wallet;
    }
}
