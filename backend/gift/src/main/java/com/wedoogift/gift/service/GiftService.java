package com.wedoogift.gift.service;

import com.wedoogift.gift.service.model.GiftModel;
import org.springframework.stereotype.Service;

@Service
public class GiftService {

    private final CompanyService companyService;
    private final DistributionService distributionService;
    private final UserService userService;

    public GiftService(CompanyService companyService, DistributionService distributionService, UserService userService) {
        this.companyService = companyService;
        this.distributionService = distributionService;
        this.userService = userService;
    }

    public GiftModel getGift() {
        GiftModel giftModel = new GiftModel();
        giftModel.setCompanies(companyService.findAllCompanies());
        giftModel.setUsers(userService.findAllUsers());
        giftModel.setDistributions(distributionService.findAllDistributions());
        return giftModel;
    }




}
