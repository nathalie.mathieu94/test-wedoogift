package com.wedoogift.gift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class GiftApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiftApplication.class, args);
	}

}
