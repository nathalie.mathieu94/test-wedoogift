package com.wedoogift.gift.repository;

import com.wedoogift.gift.repository.model.BalanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BalanceRepository extends JpaRepository<BalanceEntity, Integer> {
}
