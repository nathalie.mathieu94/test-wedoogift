package com.wedoogift.gift.service.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class DistributionModel {
    private Integer id;
    private BigDecimal amount;
    private LocalDate startDate;
    private LocalDate endDate;
    private int companyId;
    private int userId;
    private int walletId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DistributionModel that = (DistributionModel) o;
        return id == that.id && companyId == that.companyId && userId == that.userId && walletId == that.walletId && Objects.equals(amount, that.amount) && Objects.equals(startDate, that.startDate) && Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amount, startDate, endDate, companyId, userId, walletId);
    }
}
