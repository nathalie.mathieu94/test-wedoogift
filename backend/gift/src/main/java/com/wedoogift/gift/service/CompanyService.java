package com.wedoogift.gift.service;

import com.wedoogift.gift.controller.request.CompanyRequest;
import com.wedoogift.gift.repository.CompanyRepository;
import com.wedoogift.gift.repository.model.CompanyEntity;
import com.wedoogift.gift.service.model.CompanyModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public CompanyModel addCompany(CompanyRequest request) {
        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setName(request.getName());
        companyEntity.setBalance(request.getBalance());

        return toModel(companyRepository.save(companyEntity));
    }

    public List<CompanyModel> findAllCompanies() {
        List<CompanyEntity> companyEntities = companyRepository.findAll();
        return companyEntities.stream()
                .map(this::toModel)
                .collect(Collectors.toList());
    }

    private CompanyModel toModel(CompanyEntity companyEntity) {
        CompanyModel companyModel = new CompanyModel();
        companyModel.setId(companyEntity.getId());
        companyModel.setName(companyEntity.getName());
        companyModel.setBalance(companyEntity.getBalance());
        return companyModel;
    }
}
