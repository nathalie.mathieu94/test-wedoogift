package com.wedoogift.gift.service.model;

import java.util.Arrays;
import java.util.Optional;

public enum WalletType {
    GIFT(1, "gift cards"),
    FOOD(2, "food cards");

    private int id;
    private String name;

    WalletType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Optional<WalletType> findById(int id) {
        return Arrays.stream(WalletType.values()).filter(w -> w.id == id).findFirst();
    }
}
