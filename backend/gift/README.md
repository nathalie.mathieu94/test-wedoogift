# Wedoogift Backend challenge

L'application est réalisée avec spring-boot. Une base H2 en mémoire est utilisée pour la persistence des données

## Lancer l'application
 `mvn spring-boot:run`
 
## API
Une fois l'application démarrée, l'API est disponible via l'url : http://localhost:8080/swagger-ui.html

Pour s'authentifier, utiliser "user / password"